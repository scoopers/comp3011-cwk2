import requests
import re, copy
from bs4 import BeautifulSoup, Comment
import time
import pickle

class InvertedIndex():
    def __init__(self):
        self.documents = []
        self.index = dict()

    def add_to_index(self, term, docID):
        # check if term is already there, if it is, just append
        # the document ID to the list of document IDs
        # dictionary set out like so {"term": [(docID,wordCount),..,..]}
        if term in self.index:
            # if the word and document have been logged, add to word count
            if docID in self.index[term]:
                self.index[term][docID] = self.index[term][docID] + 1
                return
            self.index[term][docID] = 1
        else:
            self.index[term] = {docID:1}

    def parse_document(self, document_content, docID):
        soup = BeautifulSoup(document_content, features="html.parser")
        # remove tags which sometimes contain things that remain when calling
        # .get_text
        for script in soup(["script", "style"]):
            script.decompose()
        term_list = []
        # we're only really interested in this stuff
        tags = soup.findAll(['p', 'li', 'tr', 'h1', 'h2', 'h3'])
        for tag in tags:
            # remove special characters, we only want text
            newtext = re.sub('[^a-zA-Z0-9]+', ' ',tag.text)
            # combine lists and remove duplicates
            term_list = term_list + newtext.split()
        for term in term_list:
            self.add_to_index(term, docID)
        return

    def make_index(self, seed_url):
        to_crawl = []
        to_crawl.append(seed_url)
        temp_time = 0
        tick_num = 1
        # we're basically performing a depth-first-search here, parsing
        # the documents as we crawl, avoiding duplicate links and
        # waiting a minimum of 5 seconds between each request (time is
        # used to perform parsing and the rest made up to 5 seconds)
        while to_crawl:
            top_url = to_crawl.pop(0)
            # get time now, after stuff has been executed previous loop
            time_now = time.time()
            # calculate difference
            diff = temp_time - time_now
            # if some time is still left to make up 5 seconds, wait!
            if diff > 0:
                time.sleep(diff)
            r = requests.get(top_url)
            # calculate waiting time from now
            temp_time = time.time() + 5
            # show what web pages are being crawled (basically for debugging but is useful)
            print(f"Crawling page {tick_num} @ {top_url}")
            tick_num+=1
            # add to crawled list (which also serves as a reference to our documents)
            self.documents.append(top_url)
            content = r.content

            # feed into parsing function
            self.parse_document(content, self.documents.index(top_url))

            linkSoup = BeautifulSoup(content, features="html.parser")
            # stacking links to queue
            for link in linkSoup.findAll('a', attrs={'href': re.compile("^/")}):
                url = link.get('href')
                # ignore all pages for to do with editing, user auth and redirects
                # it just clogs up the crawler and takes too long
                if any(substr in url for substr in ['user', 'iso', 'edit']):
                    continue
                # try to avoid duplicates as well as possible
                if not any(url in ustr for ustr in to_crawl): # check if it is already queued
                    if not any(url in ustr for ustr in self.documents): # check if it has already been crawled
                        to_crawl.append(seed_url + url)

        print(f"\n{tick_num} web pages crawled.")
        # save dictionary (index) to file
        self.save_index_to_file()
        return

    def save_index_to_file(self):
        # open two files and save content to each
        with open('iindex', 'wb') as f:
            pickle.dump(self.index, f, pickle.HIGHEST_PROTOCOL)
        with open('docList', 'wb') as f:
            pickle.dump(self.documents, f, pickle.HIGHEST_PROTOCOL)
        print("Saved index and doclist.")

    def load_index_from_file(self):
        # open two files and load content from each
        try:
            with open('iindex', 'rb') as f:
                self.index = pickle.load(f)
            with open('docList', 'rb') as f:
                self.documents = pickle.load(f)
            print("Loaded index and doclist.")
        except:
            print("No such file exists, build first!")

    def print_index(self, term):
        if not self.index:
            return False
        # if no search term provided, print the whole index
        if term == '':
            for entry in self.index:
                print(f"{entry}: {self.index[entry]}")
        else:
        # else split the terms and show indicies for both
            filters = term.split()
            for term in filters:
                if not term in self.index:
                    print(f"\'{term}\' not in index.")
                    continue
                print(f"{term}: {self.index[term]}")
        return True

    def docs_for_terms(self, term):
        # rework this please, wont work with dictionaries!
        # sanity checks
        if not self.index:
            return False
        if term == '':
            return False
        # if multiple terms split them
        term_list = term.split()
        query_docs = []
        all_doc_lists = []
        # create a list of lists of document IDs
        for term in term_list:
            if term not in self.index:
                continue
            all_doc_lists.append(self.index[term].keys())
        if not all_doc_lists:
            return False
        # get the result of the intersection of all these lists
        # which is the documents that contain those terms together
        query_docs=list(set(all_doc_lists[0]).intersection(*all_doc_lists[1:]))
        query_docs.sort()
        # print the results by refering to the documents list
        print("Results:\n")
        [print(f"ID:{doc} - {self.documents[doc]}") for doc in query_docs]
        print("")
        return True


def main():
    # some tests
    ii = InvertedIndex()
    ii.add_to_index("word", 1)
    ii.add_to_index("word", 1)
    ii.add_to_index("word", 2)
    ii.add_to_index("word", 2)
    ii.add_to_index("word", 2)

    ii.add_to_index("anotherword", 2)
    print(ii.index)

    r = requests.get("http://example.webscraping.com/places/default/view/Afghanistan-1")
    ii.parse_document(r.content, 2)

    print(ii.index)



if __name__ == '__main__':
    main()
