from cmd import Cmd
import requests
import re
from bs4 import BeautifulSoup
from InvertedIndex import InvertedIndex
import time

class Prompt(Cmd):
    def __init__(self):
        Cmd.__init__(self)
        self.ii = InvertedIndex()
        # base/seed url
        self.url = "http://example.webscraping.com"

    prompt = 'Crawler >> '
    intro = '\nCrawler for comp3011 web services.\nType \'help\' or \'?\' to list commands\n'

    def do_build(self, inp):
        '''Crawls the website, creates and saves an inverted index. Note: this takes a long time.'''
        if self.ii.documents:
            print("Cannot build twice!.")
            return
        self.ii.make_index(self.url)


    def do_load(self, inp):
        '''Loads an inverted index from file.'''
        if self.ii.index:
            print("Index already loaded!.")
            return
        self.ii.load_index_from_file()

    def do_print(self, inp):
        '''Prints the inverted index for a particular word.'''
        if not self.ii.print_index(inp):
            print("Index is empty or cannot be displayed.")

    def do_find(self, inp):
        '''Takes a query parameter and retrieves pages based on its content.'''
        if not self.ii.docs_for_terms(inp):
            print("Index is empty, cannot be displayed, search term invalid, or no input provided.")

    def do_exit(self, inp):
        '''Exits the program.'''
        print("Goodbye!")
        return True

    # if the user presses Ctrl^D
    do_EOF = do_exit

def main():
    Prompt().cmdloop()

if __name__ == '__main__':
    main()
